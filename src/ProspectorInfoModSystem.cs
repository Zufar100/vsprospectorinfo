﻿using Foundation.ModConfig;
using HarmonyLib;
using ProspectorInfo.Map;
using System;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.GameContent;
using vsprospectorinfo.src;

namespace ProspectorInfo
{
    public class ProspectorInfoModSystem : ModSystem
    {
        public override bool ShouldLoad(EnumAppSide side) => side == EnumAppSide.Client;

        private const string MapLayerName = "prospectorInfo";
        public ModConfig Config;

        public override void StartClientSide(ICoreClientAPI api)
        {
            this.Config = api.LoadOrCreateConfig<ModConfig>(this);

            var mapManager = api.ModLoader.GetModSystem<WorldMapManager>();
            mapManager.RegisterMapLayer<ProspectorOverlayLayer>(MapLayerName);
        }
    }
}